use doip::claim::Claim;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Prepare the identity claim
    let service_uri = String::from("dns:yarmo.eu?type=TXT");
    let proof_uri = String::from("openpgp4fpr:9f0048ac0b23301e1f77e994909f6bd6f80f485d");
    let claim = Claim::new(service_uri, proof_uri);

    // Find matching service providers
    let matches = claim.find_matches();

    // Verify the claim
    let result = claim.verify_with_matches(matches.unwrap()).await.unwrap();

    // Print the result
    println!("Claim: {:#?}", claim);
    println!("Result: {:#?}", result);

    Ok(())
}
