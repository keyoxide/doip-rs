use doip::claim::Claim;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Prepare the identity claim
    let service_uri = String::from("https://git.sr.ht/~quaff/keyoxide_proof");
    let proof_uri = String::from("openpgp4fpr:5EB2BA392F661648A1E04A45E1BF1FDE24A291D4");
    let claim = Claim::new(service_uri, proof_uri);

    // Find matching service providers
    let matches = claim.find_matches();

    // Verify the claim
    let result = claim.verify_with_matches(matches.unwrap()).await.unwrap();

    // Print the result
    println!("Claim: {:#?}", claim);
    println!("Result: {:#?}", result);

    Ok(())
}
