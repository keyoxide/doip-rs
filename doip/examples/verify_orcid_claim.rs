use doip::claim::Claim;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Prepare the identity claim
    let service_uri = String::from("https://orcid.org/0000-0003-0110-0727");
    let proof_uri = String::from("openpgp4fpr:3637202523e7c1309ab79e99ef2dc5827b445f4b");
    let claim = Claim::new(service_uri, proof_uri);

    // Find matching service providers
    let matches = claim.find_matches();

    // Verify the claim
    let result = claim.verify_with_matches(matches.unwrap()).await.unwrap();

    // Print the result
    println!("Claim: {:#?}", claim);
    println!("Result: {:#?}", result);

    Ok(())
}
