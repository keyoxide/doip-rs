use doip::claim::Claim;
use doip::service_provider::ServiceProvider;

#[test]
fn it_accepts_discourse_matching_claims() {
    let service_uri = String::from("https://domain.org/u/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("discourse").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_discourse_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("discourse").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_dns_matching_claims() {
    let service_uri = String::from("dns:yarmo.eu?type=TXT");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("dns").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_dns_nonmatching_claims() {
    let service_uri = String::from("https://yarmo.eu/?type=TXT");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("dns").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_forem_matching_claims() {
    let service_uri = String::from("https://dev.to/alice/post");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("forem").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_forem_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("forem").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_gitea_matching_claims() {
    let service_uri = String::from("https://domain.org/alice/gitea_proof");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("gitea").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_gitea_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/alice/other_proof");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("gitea").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_github_gist_matching_claims() {
    let service_uri = String::from("https://gist.github.com/Alice/123456789");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("github_gist").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_github_gist_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/Alice/123456789");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("github_gist").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_github_social_matching_claims() {
    let service_uri = String::from("https://github.com/Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("github_social").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_github_social_nonmatching_claims() {
    let service_uri = String::from("https://gist.github.com/Alice/123456789");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("github_social").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_gitlab_matching_claims() {
    let service_uri = String::from("https://gitlab.freedesktop.org/Alice/gitlab_proof");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("gitlab").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_gitlab_nonmatching_claims() {
    let service_uri = String::from("https://github.com/Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("gitlab").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_hackernews_matching_claims() {
    let service_uri = String::from("https://news.ycombinator.com/user?id=Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("hackernews").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_hackernews_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/user?id=Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("hackernews").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_liberapay_matching_claims() {
    let service_uri = String::from("https://liberapay.com/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("liberapay").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_liberapay_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("liberapay").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_lichess_matching_claims() {
    let service_uri = String::from("https://lichess.org/@/Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("lichess").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_lichess_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/@/Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("lichess").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_lobsters_matching_claims() {
    let service_uri = String::from("https://lobste.rs/u/Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("lobsters").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_lobsters_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/u/Alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("lobsters").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_mastodon_matching_claims() {
    let service_uri = String::from("https://domain.org/@alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("mastodon").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_mastodon_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("mastodon").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_owncast_matching_claims() {
    let service_uri = String::from("https://live.domain.org");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("owncast").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_accepts_pleroma_matching_claims() {
    let service_uri = String::from("https://domain.org/users/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("pleroma").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_pleroma_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/alice");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("pleroma").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_reddit_matching_claims() {
    let service_uri = String::from("https://www.reddit.com/user/Alice/comments/123456/post");
    let proof_uri = String::from("openpgp4fpr:12345678901234p56789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("reddit").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_reddit_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/user/Alice/comments/123456/post");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("reddit").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_sourcehut_matching_claims() {
    let service_uri = String::from("https://git.sr.ht/~alice/keyoxide_proof");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("sourcehut").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_accepts_sourcehut_with_branch_matching_claims() {
    let service_uri = String::from("https://git.sr.ht/~alice/keyoxide_proof/tree/master");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("sourcehut").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_sourcehut_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/~alice/keyoxide_proof");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("sourcehut").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_twitter_matching_claims() {
    let service_uri = String::from("https://twitter.com/alice/status/1234567890123456789");
    let proof_uri = String::from("openpgp4fpr:12345678901234p56789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("twitter").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_twitter_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/alice/status/1234567890123456789");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("twitter").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_orcid_matching_claims() {
    let service_uri = String::from("https://orcid.org/1234-5678-9012-3456");
    let proof_uri = String::from("openpgp4fpr:12345678901234p56789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("orcid").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_orcid_nonmatching_claims() {
    let service_uri = String::from("https://domain.org/1234-5678-9012-3456");
    let proof_uri = String::from("openpgp4fpr:1234567890123456789012345678901234567890");
    let claim = Claim::new(service_uri, proof_uri);

    let sp = ServiceProvider::load("orcid").unwrap();
    assert_eq!(sp.test(&claim), false);
}
