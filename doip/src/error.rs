use miette::Diagnostic;
use thiserror::Error;

#[derive(Error, Diagnostic, Debug)]
pub enum DoipError {
    #[error("Unkown error")]
    Unknown,

    #[error("Failed to verify claim for reason: {reason:?}")]
    #[diagnostic(code(doip))]
    FailedToVerifyClaim { reason: VerificationFailureReasons },

    #[error("Claim could not be matched to any service provider")]
    #[diagnostic(code(doip))]
    NoMatchedServiceProviders,

    #[error("Failed to load service provider configs")]
    #[diagnostic(code(doip))]
    FailedToLoadServiceProviderConfigs,

    #[error("Failed to adapt service provider to claim")]
    #[diagnostic(code(doip::service_provider))]
    FailedToAdaptServiceProvider,

    #[error("Failed to search for target in proof data")]
    #[diagnostic(code(doip::proof_verification))]
    FailedToSearchTargetProofData,
}

#[derive(Debug)]
pub enum VerificationFailureReasons {
    /// All known service providers failed to verify the claim
    KnownServiceProvidersFailed,
    /// The unambiguous service provider failed to verify the claim
    UnambiguousServiceProviderFailed,
}
