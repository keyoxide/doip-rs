use crate::claim::Claim;
use crate::error::DoipError;
use crate::service_provider::ServiceProvider;
use async_recursion::async_recursion;
use regex::Regex;
use serde_json::Value;

#[derive(Debug, Deserialize)]
pub struct ProofValidationResult {
    pub result: bool,
    pub service_provider: Option<ServiceProvider>,
    pub proxy_used: Option<String>,
}

/// Attempt to detect a target string in a [String]
pub async fn try_with_string(
    string: String,
    service_provider: &ServiceProvider,
    claim: &Claim,
) -> Result<bool, DoipError> {
    // Ok(string_contains_proof(string, claim))
    process_data(&Value::String(string), service_provider, claim, 0).await
}

/// Attempt to detect a target string in a [Value], based on the path defined by a [ServiceProvider]
pub async fn try_with_json(
    object: &Value,
    service_provider: &ServiceProvider,
    claim: &Claim,
) -> Result<bool, DoipError> {
    process_data(object, service_provider, claim, 0).await
}

#[async_recursion]
async fn process_data(
    object: &Value,
    service_provider: &ServiceProvider,
    claim: &Claim,
    instruction: usize,
) -> Result<bool, DoipError> {
    if object.is_array() {
        // If the JSON data is an array, iterate over it
        let mut res = false;
        for item in object.as_array().unwrap() {
            if res {
                continue;
            }
            res = match process_data(item, service_provider, claim, instruction).await {
                Ok(res) => res,
                Err(err) => return Err(err),
            };
        }
        Ok(res)
    } else if object.is_object() {
        // If the JSON data is an object, traverse it
        let current_instruction =
            &service_provider.proof.target.as_ref().unwrap().path[instruction];
        process_data(
            &object[current_instruction],
            service_provider,
            claim,
            instruction + 1,
        )
        .await
    } else if object.is_string() {
        // If the JSON data is a string, check it
        Ok(string_contains_proof(object.as_str().unwrap_or_default().to_string(), claim).await)
    } else {
        Ok(false)
    }
}

async fn string_contains_proof(string: String, claim: &Claim) -> bool {
    // TODO Decode HTML/XML entities

    // Check for plaintext proof
    let formatted_proof = &claim.proof_uri.to_lowercase();

    if string.to_lowercase().contains(formatted_proof) {
        return true;
    }

    // Check for hashed proof
    if find_and_verify_hashed_proof(&string, formatted_proof) {
        return true;
    }

    // Check for HTTP proofs
    if find_and_verify_url_proof(&string, formatted_proof).await {
        return true;
    }

    // If all methods fail, return false
    false
}

/// Searches using Regex the `text_to_search` for argon2 and bcrypt hashes
/// If it finds a hash it will verify it against the `formatted_target_proof_uri`.
/// If verification for any found hash succeeds it will return `true`, or if none
/// match then it will return `false`.
/// All errors return `false`
fn find_and_verify_hashed_proof(text_to_search: &str, formatted_target_proof_uri: &str) -> bool {
    let re = Regex::new(r"\$(argon2(?:id|d|i)|2a|2b|2y)(?:\$[a-zA-Z0-9=+\-,./]+)+").unwrap();
    for regex_match in re.captures_iter(text_to_search) {
        match regex_match.get(1).map(|f| f.as_str()) {
            Some("2a" | "2b" | "2y") => {
                let found_hash = regex_match.get(0).unwrap().as_str();
                match bcrypt::verify(formatted_target_proof_uri, found_hash) {
                    Ok(true) => return true,
                    _ => continue,
                }
            }
            Some("argon2id" | "argon2i" | "argon2" | "argon2d") => {
                let found_hash = regex_match.get(0).unwrap().as_str();
                match argon2::verify_encoded(found_hash, formatted_target_proof_uri.as_bytes()) {
                    Ok(true) => return true,
                    _ => continue,
                }
            }
            _ => continue,
        }
    }
    false
}

/// Finds and then makes a HEAD request to any links found in the text.
/// If the HEAD response contains the header `Ariadne-Identity-Proof` header,
/// it will be checked against the provided target proof uri.
async fn find_and_verify_url_proof(text_to_search: &str, formatted_target_proof_uri: &str) -> bool {
    // This regex matches valid HTTP/HTTPS URLs of any kind
    let re = Regex::new(r"^https?://(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&/=]*)$").unwrap();
    // Iterate over any matched URL found in the provided text, looping until one works or there are no more to verify
    for url in re.find_iter(text_to_search) {
        // Make an HTTP HEAD request to the URL, following only one redirect as specified in the Ariadne spec
        let request = reqwest::Client::builder()
            .redirect(reqwest::redirect::Policy::limited(1))
            .build()
            .unwrap()
            .head(url.as_str())
            .header(
                reqwest::header::USER_AGENT,
                format!("doiprs/{}", env!("CARGO_PKG_VERSION")),
            )
            .send()
            .await;

        // Check if the request succeeded and the response contains a valid and matching ariadne-identity-proof header, or otherwise this will not return anything and will pass to the next URL match (if any)
        if let Ok(response) = request {
            // Note: here, an empty str is used as a default value should the header not exist or the header value not be valid ASCII for some reason
            let header = response
                .headers()
                .get("ariadne-identity-proof")
                .map_or("", |v| v.to_str().unwrap_or(""));
            if header.contains(formatted_target_proof_uri) {
                return true;
            }
        }
    }
    // If all URLs failed verification or there are no URLs, return false by default
    false
}

#[cfg(test)]
mod tests {
    use crate::{
        claim::Claim,
        proof_validation::{try_with_json, try_with_string},
        service_provider::ServiceProvider,
    };

    static CLAIM_URI: &str = "https://activity.pub/@alice";
    static CLAIM_FPR: &str = "openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E";
    static SP_NAME: &str = "mastodon";
    static VALID_BCRYPT_PROOF: &str =
        "$2a$11$nkO0.UIy7U/.JonOTDWVpe17xV2xdnpos1lMm.i.GiDtf8uD3hDK2";
    static VALID_ARGON_PROOF: &str =
        "$argon2id$v=19$m=64,t=512,p=2$pmQ2AWK09WBALqpQVFYXrg$sGPPklVhMAGsVwSUejqF1Q";

    static INVALID_BCRYPT_PROOF: &str =
        "$2a$11$BFjl48zLWeu6KK/R6C20gOk9wZDBif1abj/R3MHbKJ3hM6OpC6.wG";
    static INVALID_ARGON_PROOF: &str =
        "$argon2id$v=19$m=64,t=512,p=2$DHln6+1NYgw8MMBT8E42iQ$l5lWWmwZlbIrdgayU0ftzQ";

    #[tokio::test]
    async fn try_with_string_validates_valid_bcrypt_hash_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());

        assert!(try_with_string(VALID_BCRYPT_PROOF.to_string(), &sp, &claim)
            .await
            .unwrap());
    }

    #[tokio::test]
    async fn try_with_string_validates_valid_argon_hash_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());

        assert!(try_with_string(VALID_ARGON_PROOF.to_string(), &sp, &claim)
            .await
            .unwrap());
    }

    #[tokio::test]
    async fn try_with_string_validates_invalid_bcrypt_hash_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());
        assert!(
            !try_with_string(INVALID_BCRYPT_PROOF.to_string(), &sp, &claim)
                .await
                .unwrap()
        );
    }

    #[tokio::test]
    async fn try_with_string_validates_invalid_argon_hash_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());
        assert!(
            !try_with_string(INVALID_ARGON_PROOF.to_string(), &sp, &claim)
                .await
                .unwrap()
        );
    }

    static VALID_PLAINTEXT_PROOF: &str =
        "Here is my proof: openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E";
    static INVALID_PLAINTEXT_PROOF: &str =
        "Here is my proof: openpgp4fpr:3637202523E7C1309AB79E99EF2DC5827B445F4B";

    static VALID_JSON_PROOF: &str = r#"
        {
            "attachment": {
                "value": "openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E"
            }
        }"#;
    static INVALID_JSON_PROOF: &str = r#"
        {
            "value": {
                "attachment": "openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E"
            }
        }"#;

    #[tokio::test]
    async fn try_with_string_validates_valid_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());
        assert_eq!(
            try_with_string(VALID_PLAINTEXT_PROOF.to_string(), &sp, &claim)
                .await
                .unwrap(),
            true
        );
    }

    #[tokio::test]
    async fn try_with_string_rejects_invalid_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());
        assert_eq!(
            try_with_string(INVALID_PLAINTEXT_PROOF.to_string(), &sp, &claim)
                .await
                .unwrap(),
            false
        );
    }

    #[tokio::test]
    async fn try_with_json_validates_valid_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());
        assert_eq!(
            try_with_json(
                &serde_json::from_str(VALID_JSON_PROOF).unwrap(),
                &sp,
                &claim
            )
            .await
            .unwrap(),
            true
        );
    }

    #[tokio::test]
    async fn try_with_json_rejects_invalid_proof() {
        let sp = ServiceProvider::load(SP_NAME).unwrap();
        let claim = Claim::new(CLAIM_URI.to_string(), CLAIM_FPR.to_string());
        assert_eq!(
            try_with_json(
                &serde_json::from_str(INVALID_JSON_PROOF).unwrap(),
                &sp,
                &claim
            )
            .await
            .unwrap(),
            false
        );
    }
}
