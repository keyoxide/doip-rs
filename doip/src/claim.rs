use crate::error::{DoipError, VerificationFailureReasons};
use crate::proof_request_protocols;
use crate::service_provider::{self, ServiceProvider};

#[derive(Debug, Deserialize)]
pub struct Claim {
    pub service_uri: String,
    pub proof_uri: String,
}

#[derive(Debug, Deserialize)]
pub struct VerificationResult {
    pub result: bool,
    pub service_provider: Option<ServiceProvider>,
    pub proxy_used: Option<String>,
}

impl Claim {
    /// Initialize a new Claim
    ///
    /// # Example
    ///
    /// ```
    /// use doip::claim::Claim;
    ///
    /// let service_uri = String::from("dns:alice.tld?type=TXT");
    /// let proof_uri = String::from("openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E");
    /// let claim = Claim::new(service_uri, proof_uri);
    /// ```
    pub fn new(service_uri: String, proof_uri: String) -> Self {
        Claim {
            service_uri,
            proof_uri,
        }
    }

    /// Find service providers matching the Claim
    ///
    /// This is useful to update UIs while the verification is ongoing.
    ///
    /// # Example
    ///
    /// ```
    /// use doip::claim::Claim;
    ///
    /// let service_uri = String::from("dns:alice.tld?type=TXT");
    /// let proof_uri = String::from("openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E");
    /// let claim = Claim::new(service_uri, proof_uri);
    ///
    /// let matches = claim.find_matches();
    /// ```
    pub fn find_matches(&self) -> Result<Vec<ServiceProvider>, DoipError> {
        // Load the index of known service providers
        let mut all_service_providers = match service_provider::get_all() {
            Ok(v) => v,
            Err(_) => return Err(DoipError::FailedToLoadServiceProviderConfigs),
        };

        all_service_providers.retain(|service_provider| service_provider.test(self));

        match all_service_providers.is_empty() {
            true => Err(DoipError::NoMatchedServiceProviders),
            false => Ok(all_service_providers),
        }
    }

    /// Find service providers matching the Claim
    ///
    /// This is useful to update UIs while the verification is ongoing.
    ///
    /// # Example
    ///
    /// ```
    /// use doip::claim::Claim;
    /// use doip::service_provider;
    ///
    /// let service_uri = String::from("dns:alice.tld?type=TXT");
    /// let proof_uri = String::from("openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E");
    /// let claim = Claim::new(service_uri, proof_uri);
    /// let service_providers = service_provider::get_all().unwrap();
    /// let matches = claim.find_matches_with_custom_service_providers(service_providers);
    /// ```
    pub fn find_matches_with_custom_service_providers(
        &self,
        mut service_providers: Vec<ServiceProvider>,
    ) -> Result<Vec<ServiceProvider>, DoipError> {
        service_providers.retain(|service_provider| service_provider.test(self));

        match service_providers.is_empty() {
            true => Err(DoipError::NoMatchedServiceProviders),
            false => Ok(service_providers),
        }
    }

    /// Verify a Claim
    ///
    /// # Example
    ///
    /// ```
    /// use doip::claim::Claim;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let service_uri = String::from("dns:alice.tld?type=TXT");
    ///     let proof_uri = String::from("openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E");
    ///     let claim = Claim::new(service_uri, proof_uri);
    ///
    ///     let result = claim.verify().await;
    /// }
    /// ```
    pub async fn verify(&self) -> Result<VerificationResult, DoipError> {
        match self.find_matches() {
            Ok(matched_service_providers) => {
                self.verify_with_matches(matched_service_providers).await
            }
            Err(e) => Err(e),
        }
    }

    /// Verify a Claim with a predefined list of matching service providers
    ///
    /// # Example
    ///
    /// ```
    /// use doip::claim::Claim;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let service_uri = String::from("dns:alice.tld?type=TXT");
    ///     let proof_uri = String::from("openpgp4fpr:EB85BB5FA33A75E15E944E63F231550C4F47E38E");
    ///     let claim = Claim::new(service_uri, proof_uri);
    ///
    ///     let matches = claim.find_matches();
    ///     let result = claim.verify_with_matches(matches.unwrap()).await;
    /// }
    /// ```
    pub async fn verify_with_matches(
        &self,
        matched_service_providers: Vec<ServiceProvider>,
    ) -> Result<VerificationResult, DoipError> {
        let mut verification_result: Option<VerificationResult> = None;

        // For each known service provider
        for service_provider_raw in matched_service_providers {
            let service_provider = match service_provider_raw.adapt_to_claim(self) {
                Ok(result) => result,
                Err(_) => continue,
            };

            // If the verification was already done, skip this one
            if verification_result.is_some() {
                continue;
            }

            // Test the claim's URI against the service provider and see if they are a match
            let service_provider_match = service_provider.test(self);

            // Skip the service provider if they don't match to the claim's URI
            if !service_provider_match {
                continue;
            }

            // Get the request protocol based on the service provider
            let request_protocol = proof_request_protocols::get_request_protocol(
                &service_provider.proof.request.protocol,
            );

            // Perform the actual verification
            let is_verified_by_service_provider =
                request_protocol.verify_claim(self, &service_provider).await;

            // Skip the service provider if the verification process was unsuccessful
            if is_verified_by_service_provider.is_none() {
                continue;
            }

            // If the service provide verified the claim, update the claim's data accordingly
            if is_verified_by_service_provider.unwrap() {
                verification_result = Some(VerificationResult {
                    result: true,
                    proxy_used: None,
                    service_provider: Some(service_provider),
                });
            }
        }

        // If no service provider verified the claim, update claim's data accordingly
        if verification_result.is_none() {
            return Err(DoipError::FailedToVerifyClaim {
                reason: VerificationFailureReasons::KnownServiceProvidersFailed,
            });
        }

        Ok(verification_result.unwrap())
    }
}
