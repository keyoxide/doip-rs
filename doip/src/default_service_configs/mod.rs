use std::collections::HashMap;

use crate::error::DoipError;

pub struct DefaultServiceConfigs {
    pub configs: HashMap<&'static str, &'static str>,
}

impl DefaultServiceConfigs {
    fn load() -> DefaultServiceConfigs {
        let mut configs: HashMap<&str, &str> = HashMap::new();

        #[cfg(feature = "discourse")]
        configs.insert("discourse", include_str!("discourse.toml"));

        #[cfg(feature = "dns")]
        configs.insert("dns", include_str!("dns.toml"));

        #[cfg(feature = "forem")]
        configs.insert("forem", include_str!("forem.toml"));

        #[cfg(feature = "gitea")]
        configs.insert("gitea", include_str!("gitea.toml"));

        #[cfg(feature = "github_gist")]
        configs.insert("github_gist", include_str!("github_gist.toml"));

        #[cfg(feature = "github_social")]
        configs.insert("github_social", include_str!("github_social.toml"));

        #[cfg(feature = "gitlab")]
        configs.insert("gitlab", include_str!("gitlab.toml"));

        #[cfg(feature = "hackernews")]
        configs.insert("hackernews", include_str!("hackernews.toml"));

        #[cfg(feature = "liberapay")]
        configs.insert("liberapay", include_str!("liberapay.toml"));

        #[cfg(feature = "lichess")]
        configs.insert("lichess", include_str!("lichess.toml"));

        #[cfg(feature = "lobsters")]
        configs.insert("lobsters", include_str!("lobsters.toml"));

        #[cfg(feature = "mastodon")]
        configs.insert("mastodon", include_str!("mastodon.toml"));

        #[cfg(feature = "opencollective")]
        configs.insert("opencollective", include_str!("opencollective.toml"));

        #[cfg(feature = "orcid")]
        configs.insert("orcid", include_str!("orcid.toml"));

        #[cfg(feature = "owncast")]
        configs.insert("owncast", include_str!("owncast.toml"));

        #[cfg(feature = "pleroma")]
        configs.insert("pleroma", include_str!("pleroma.toml"));

        #[cfg(feature = "reddit")]
        configs.insert("reddit", include_str!("reddit.toml"));

        #[cfg(feature = "sourcehut")]
        configs.insert("sourcehut", include_str!("sourcehut.toml"));

        #[cfg(feature = "twitter")]
        configs.insert("twitter", include_str!("twitter.toml"));

        DefaultServiceConfigs { configs }
    }

    pub fn get_config(&self, name: &str) -> Result<&'static str, DoipError> {
        match self.configs.get(name) {
            Some(config) => Ok(config),
            None => Err(DoipError::NoMatchedServiceProviders),
        }
    }
}

lazy_static! {
    pub static ref DEFAULT_CONFIG: DefaultServiceConfigs = DefaultServiceConfigs::load();
}
