#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;

pub mod claim;
pub mod error;
pub mod service_provider;

mod default_service_configs;
mod proof_request_protocols;
mod proof_validation;
