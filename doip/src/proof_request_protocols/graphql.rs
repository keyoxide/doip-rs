use crate::claim::Claim;
use crate::proof_validation;
use crate::service_provider::ServiceProvider;
use async_trait::async_trait;
use serde_json::Value;

use super::RequestProtocol;

pub struct GraphQLRequestProtocol;

#[async_trait]
impl RequestProtocol for GraphQLRequestProtocol {
    async fn verify_claim(
        &self,
        claim: &Claim,
        service_provider: &ServiceProvider,
    ) -> Option<bool> {
        // Prepare the proof request URI by replacing its regex placeholders
        let proof_request_uri = service_provider
            .proof
            .request
            .uri
            .as_ref()
            .expect("Request URI for protocol http should be Some");
        service_provider.proof.request.graphql.as_ref()?;

        // Fetch the uri
        let client = reqwest::Client::new();
        let request = client
            .post(proof_request_uri)
            .header(
                reqwest::header::USER_AGENT,
                format!("doiprs/{}", env!("CARGO_PKG_VERSION")),
            )
            .header(reqwest::header::CONTENT_TYPE, "application/json")
            .body(
                service_provider
                    .proof
                    .request
                    .graphql
                    .as_ref()
                    .unwrap()
                    .query
                    .clone(),
            );

        // Fetch the JSON document
        let response = request.send().await;

        // Handle request errors and parse JSON
        let parsed = match response {
            Ok(res) => res.json::<Value>().await,
            Err(_) => return None,
        };

        // Handle parsing errors or return verification
        let returned_data = match &parsed {
            Ok(res) => res,
            Err(_) => return None,
        };

        // Attempt to validate the proof
        match proof_validation::try_with_json(returned_data, service_provider, claim).await {
            Ok(res) => Some(res),
            Err(_) => None,
        }
    }
}
