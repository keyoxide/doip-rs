use crate::claim::Claim;
use crate::proof_validation;
use crate::service_provider::ServiceProvider;

use std::collections::HashMap;

use async_trait::async_trait;

use trust_dns_resolver::TokioAsyncResolver;
use url::Url;

use super::RequestProtocol;

pub struct DnsRequestProtocol;

#[async_trait]
impl RequestProtocol for DnsRequestProtocol {
    async fn verify_claim(
        &self,
        claim: &Claim,
        service_provider: &ServiceProvider,
    ) -> Option<bool> {
        let resolver = TokioAsyncResolver::tokio_from_system_conf()
            .expect("unable to create TokioAsyncResolver for DNS resolution");
        let parsed_uri = match Url::parse(claim.service_uri.as_str()) {
            Ok(url) => url,
            Err(_) => return None,
        };

        // Check if scheme is not DNS, or if the type is not TXT (only type supported for now)
        let query_pairs = parsed_uri.query_pairs().collect::<HashMap<_, _>>();
        if parsed_uri.scheme() != "dns"
            || query_pairs
                .get("type")
                .map(|t| t.to_lowercase() != "txt")
                .unwrap_or(false)
        {
            return None;
        }

        // Lookup the TXT records of the uri
        let response = resolver.txt_lookup(parsed_uri.path()).await;

        if let Ok(response) = response {
            // Loop over records
            for record in response.iter().map(|i| i.to_string()) {
                if let Ok(res) =
                    proof_validation::try_with_string(record, service_provider, claim).await
                {
                    if res {
                        return Some(res);
                    }
                }
            }
        }

        // If a match was not found in any of the TXT records, or the lookup failed, return false
        Some(false)
    }
}
