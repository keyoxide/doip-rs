use crate::claim::Claim;
use crate::proof_validation;
use crate::service_provider::ServiceProvider;
use async_trait::async_trait;
use serde_json::Value;

use super::RequestProtocol;

pub struct HttpRequestProtocol;

#[async_trait]
impl RequestProtocol for HttpRequestProtocol {
    async fn verify_claim(
        &self,
        claim: &Claim,
        service_provider: &ServiceProvider,
    ) -> Option<bool> {
        // Prepare the proof request URI by replacing its regex placeholders
        let proof_request_uri = service_provider
            .proof
            .request
            .uri
            .as_ref()
            .expect("Request URI for protocol http should be Some");

        // Fetch the uri
        let client = reqwest::Client::new();
        let request = client.get(proof_request_uri).header(
            reqwest::header::USER_AGENT,
            format!("doiprs/{}", env!("CARGO_PKG_VERSION")),
        );

        match service_provider.proof.response.format.as_str() {
            "json" => {
                // Fetch the JSON document
                let response = request
                    .header(reqwest::header::ACCEPT, "application/json")
                    .send()
                    .await;

                // Handle request errors and parse JSON
                let parsed = match response {
                    Ok(res) => res.json::<Value>().await,
                    Err(_) => return None,
                };

                // Handle parsing errors or return verification
                let returned_data = match &parsed {
                    Ok(res) => res,
                    Err(_) => return None,
                };

                // Attempt to validate the proof
                match proof_validation::try_with_json(returned_data, service_provider, claim).await
                {
                    Ok(res) => Some(res),
                    Err(_) => None,
                }
            }
            "plaintext" => {
                // Fetch the plain text document
                let response = request.send().await;

                // Handle request errors and parse plain text
                let parsed = match response {
                    Ok(res) => res.text().await,
                    Err(_) => return None,
                };

                // Handle parsing errors or return verification
                let returned_data = match &parsed {
                    Ok(res) => res,
                    Err(_) => return None,
                };

                // Attempt to validate the proof
                match proof_validation::try_with_string(
                    returned_data.to_string(),
                    service_provider,
                    claim,
                )
                .await
                {
                    Ok(res) => Some(res),
                    Err(_) => None,
                }
            }
            _ => None,
        }
    }
}
