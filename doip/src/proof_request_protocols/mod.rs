use async_trait::async_trait;

use crate::{claim::Claim, service_provider::ServiceProvider};

use self::{dns::DnsRequestProtocol, graphql::GraphQLRequestProtocol, http::HttpRequestProtocol};

pub mod dns;
pub mod graphql;
pub mod http;

#[async_trait]
pub trait RequestProtocol {
    async fn verify_claim(&self, claim: &Claim, service_provider: &ServiceProvider)
        -> Option<bool>;
}

/// Fetches a [RequestProtocol] based on a simple name (such as http or dns)
pub fn get_request_protocol(name: &str) -> Box<dyn RequestProtocol> {
    match name {
        "dns" => Box::new(DnsRequestProtocol),
        "graphql" => Box::new(GraphQLRequestProtocol),
        "http" => Box::new(HttpRequestProtocol),
        _ => panic!("Invalid request protocol name"),
    }
}
