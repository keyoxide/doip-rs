use crate::{claim::Claim, default_service_configs::DEFAULT_CONFIG, error::DoipError};
use config::{Config, ConfigError, File};
use regex::Regex;
use std::collections::BTreeMap;

#[derive(Debug, Deserialize)]
pub struct ServiceProviderIndex {
    pub service_providers: Vec<ServiceProviderIndexItem>,
}
#[derive(Debug, Deserialize)]
pub struct ServiceProviderIndexItem {
    pub name: String,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct ServiceProvider {
    pub about: SPAbout,
    pub profile: SPProfile,
    pub claim: SPClaim,
    pub proof: SPProof,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPAbout {
    pub name: String,
    pub shortname: String,
    pub homepage: Option<String>,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPProfile {
    pub display: Option<String>,
    pub uri: Option<String>,
    pub qr: Option<String>,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPClaim {
    pub uri_is_ambiguous: bool,
    pub uri: String,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPProof {
    pub request: SPProofRequest,
    pub response: SPProofResponse,
    pub target: Option<SPProofTarget>,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPProofRequest {
    pub uri: Option<String>,
    pub protocol: String,
    pub access_restrictions: Option<String>,
    pub graphql: Option<SPProofRequestGraphQL>,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPProofRequestGraphQL {
    pub query: String,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPProofResponse {
    pub format: String,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct SPProofTarget {
    pub relation: String,
    pub path: Vec<String>,
}

impl ServiceProvider {
    pub fn load(name: &str) -> Result<Self, DoipError> {
        let mut s = Config::default();
        s.merge(File::from_str(
            DEFAULT_CONFIG.get_config(name)?,
            config::FileFormat::Toml,
        ))
        .map_err(|_| DoipError::FailedToLoadServiceProviderConfigs)?;

        s.try_into()
            .map_err(|_| DoipError::FailedToLoadServiceProviderConfigs)
    }

    /// Load ServiceProvider from TOML formatted string
    pub fn load_from_str(config_str: &str) -> Result<Self, DoipError> {
        let mut s = Config::default();
        s.merge(File::from_str(config_str, config::FileFormat::Toml))
            .map_err(|_| DoipError::FailedToLoadServiceProviderConfigs)?;

        s.try_into()
            .map_err(|_| DoipError::FailedToLoadServiceProviderConfigs)
    }

    pub fn test(&self, claim: &Claim) -> bool {
        // Use regular expression to see if a claim's URI matches the service provider
        Regex::new(&self.claim.uri)
            .unwrap()
            .is_match(&claim.service_uri)
    }
    pub fn adapt_to_claim(&self, claim: &Claim) -> Result<ServiceProvider, DoipError> {
        // Create a mutable service provider object
        let mut new_service_provider = ServiceProvider::load(&self.about.shortname).unwrap();

        // Prepare the template variables
        let mut template_variables: BTreeMap<String, upon::Value> = BTreeMap::new();

        // Prepare the claim_uri template variables
        let mut claim_uri_tvar: BTreeMap<String, upon::Value> = BTreeMap::new();
        let mut claim_uri_parts_tvar: Vec<upon::Value> = Vec::new();

        // Prepare the regex object for the claim's URI
        let re = match Regex::new(&self.claim.uri) {
            Ok(re) => re,
            Err(_) => return Err(DoipError::FailedToAdaptServiceProvider),
        };

        // For each regex found, add it to the template variables
        for cap in re.captures_iter(&claim.service_uri) {
            for (i, subcap) in cap.iter().enumerate() {
                if i == 0 {
                    claim_uri_tvar.insert(
                        "full".to_owned(),
                        upon::Value::String(subcap.unwrap().as_str().to_string()),
                    );
                    continue;
                }
                if subcap.is_none() {
                    continue;
                }
                claim_uri_parts_tvar
                    .push(upon::Value::String(subcap.unwrap().as_str().to_string()));
            }
        }

        // adding default filter allows for unused parts, to prevent errors
        // count all claim parts and add empty strings if tvar length is less
        let claim_uri_parts_tmpl = Regex::new(r"\{\{ claim_uri.parts.(\d+) ")
            .unwrap()
            .captures_iter(&new_service_provider.proof.request.uri.as_ref().unwrap()).collect::<Vec<regex::Captures>>();
        while claim_uri_parts_tvar.len() < claim_uri_parts_tmpl.len() {
            claim_uri_parts_tvar.push(upon::Value::String("".to_string()));
        }

        claim_uri_tvar.insert("parts".to_string(), upon::Value::List(claim_uri_parts_tvar));
        template_variables.insert("claim_uri".to_string(), upon::Value::Map(claim_uri_tvar));

        // Prepare the templating engine
        let mut engine = upon::Engine::new();
        engine.add_filter("lower", str::to_lowercase);
        engine.add_filter("upper", str::to_uppercase);
        engine.add_filter("url_encode", |s: String| {
            urlencoding::encode(&s).to_string()
        });
        engine.add_filter(
            "default",
            |s: String, default: String| {
                if s.is_empty() {
                    default
                } else {
                    s
                }
            },
        );
        let ctx = upon::Value::Map(template_variables);

        // Render the relevant string
        new_service_provider.profile.display = match self.evaluate_template(
            &engine,
            &ctx,
            new_service_provider.profile.display.as_ref().unwrap(),
        ) {
            Some(string) => Some(string),
            None => return Err(DoipError::FailedToAdaptServiceProvider),
        };
        new_service_provider.profile.uri = match self.evaluate_template(
            &engine,
            &ctx,
            new_service_provider.profile.uri.as_ref().unwrap(),
        ) {
            Some(string) => Some(string),
            None => return Err(DoipError::FailedToAdaptServiceProvider),
        };
        new_service_provider.proof.request.uri = match self.evaluate_template(
            &engine,
            &ctx,
            new_service_provider.proof.request.uri.as_ref().unwrap(),
        ) {
            Some(string) => Some(string),
            None => return Err(DoipError::FailedToAdaptServiceProvider),
        };

        if new_service_provider.proof.request.graphql.is_some() {
            new_service_provider
                .proof
                .request
                .graphql
                .as_mut()
                .unwrap()
                .query = match self.evaluate_template(
                &engine,
                &ctx,
                &new_service_provider
                    .proof
                    .request
                    .graphql
                    .as_ref()
                    .unwrap()
                    .query,
            ) {
                Some(string) => string,
                None => return Err(DoipError::FailedToAdaptServiceProvider),
            };
        }

        Ok(new_service_provider)
    }
    fn evaluate_template(
        &self,
        engine: &upon::Engine,
        ctx: &upon::Value,
        string: &str,
    ) -> Option<String> {
        let template = match engine.compile(string) {
            Ok(re) => re,
            Err(_) => return None,
        };
        match template.render(ctx) {
            Ok(result) => Some(result),
            Err(_) => None,
        }
    }
}

/// Get all known service providers
///
/// # Example
///
/// ```
/// use doip::service_provider;
///
/// let all_service_providers = service_provider::get_all();
/// ```
pub fn get_all() -> Result<Vec<ServiceProvider>, ConfigError> {
    // Init the output vector
    let mut service_providers: Vec<ServiceProvider> = Vec::new();

    // For each known service provider in DEFAULT_CONFIG
    for name in DEFAULT_CONFIG.configs.keys() {
        service_providers.push(ServiceProvider::load(name).unwrap());
    }

    Ok(service_providers)
}

#[cfg(test)]
mod tests {
    use super::get_all;

    #[test]
    fn load_all_known_service_providers() {
        let all_service_providers = get_all();
        assert_eq!(all_service_providers.is_ok(), true);
        assert_eq!(all_service_providers.unwrap_or_default().len() > 0, true);
    }
}
