use miette::Diagnostic;
use thiserror::Error;

#[derive(Error, Diagnostic, Debug)]
pub enum DoipError {
    #[error("Invalid Key: {fingerprint:?} for reason: {reason:?}")]
    #[diagnostic(code(doip_openpgp))]
    InvalidKey {
        fingerprint: String,
        reason: InvalidKeyReasons,
    },

    #[error("Failed to parse given key")]
    #[diagnostic(code(doip_openpgp::read_key_from_string))]
    FailedToParseKey,

    #[error("Unkown error")]
    Unknown,

    #[error("Failed to fetch key via {method:?} from {location:?}")]
    #[diagnostic(code(doip_openpgp))]
    FailedToFetchKey { method: String, location: String },

    #[error("Failed to parse key identifier")]
    #[diagnostic(code(doip_openpgp))]
    FailedToParseKeyIdentifier,
}

impl DoipError {
    pub fn new_failed_to_fetch_key_wkd(email_address: &str) -> Self {
        let location = if let Some((_, domain)) = email_address.split_once('@') {
            domain.to_string()
        } else {
            "N/A".to_string()
        };

        DoipError::FailedToFetchKey {
            method: "wkd".to_string(),
            location,
        }
    }
}

#[derive(Debug)]
pub enum InvalidKeyReasons {
    /// The key was revoked or expired
    KeyIsRevokedOrExpired,
    /// The key does not conform to sequoia_openpgp::policy::StandardPolicy or has a Revoked Self Signature
    KeyHasRevokedSelfSignOrNonConforming,
}
