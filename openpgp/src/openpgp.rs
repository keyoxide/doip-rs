use sequoia_net::KeyServer;
use sequoia_openpgp::packet::UserID;
use sequoia_openpgp::policy::StandardPolicy;
use sequoia_openpgp::{Cert, KeyID};
use std::collections::HashMap;
use std::{str, time};

use crate::error::DoipError;
use crate::error::InvalidKeyReasons;
/// Returns a `HashMap<UserID,Vec<String>>` of doip proofs where the key is `UserID`, for a given `key`
/// It also validates the key before looking for notations.
/// # Arguments
///
/// * `key` - A reference to a `Cert` containing a public key with notations
///
/// # Example
///
/// ```
/// use std::collections::HashMap;
/// use doip_openpgp::openpgp::read_key_from_string;
/// use doip_openpgp::openpgp::get_keys_doip_proofs;
/// use sequoia_openpgp::packet::UserID;
///
/// let mut key = read_key_from_string(r###"-----BEGIN PGP PUBLIC KEY BLOCK-----
/// Comment: Alice's OpenPGP certificate with added notations and proofs
///    
/// mDMEXEcE6RYJKwYBBAHaRw8BAQdArjWwk3FAqyiFbFBKT4TzXcVBqPTB3gmzlC/U
/// b7O1u120JkFsaWNlIExvdmVsYWNlIDxhbGljZUBvcGVucGdwLmV4YW1wbGU+iPIE
/// ExYIAJoCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTrhbtfozp14V6UTmPy
/// MVUMT0fjjgUCY/inQSsUgAAAAAAXAAtub3RfYV9wcm9vZkBub3RfYV9wcm9vZm5v
/// dF9hX3Byb29mNRSAAAAAABAAHHByb29mQGFyaWFkbmUuaWRkbnM6b3BlbnBncC5l
/// eGFtcGxlP3R5cGU9VFhUAAoJEPIxVQxPR+OOghcA/3jrFay1wXhGRcc6V6BvnpXZ
/// /kNQvpjWXpWUnQ4jxpR0AP9RgvBLNETvyHb/eA4k9PS0w4VumSGPE+cCeMMVnXvw
/// Arg4BFxHBOkSCisGAQQBl1UBBQEBB0BC/wYhratJPOCptcKkMNgyIpFWK0KzLbTf
/// HewT356+IgMBCAeIeAQYFggAIBYhBOuFu1+jOnXhXpROY/IxVQxPR+OOBQJcRwTp
/// AhsMAAoJEPIxVQxPR+OOWdABAMUdSzpMhzGs1O0RkWNQWbUzQ8nUOeD9wNbjE3zR
/// +yfRAQDbYqvtWQKN4AQLTxVJN5X5AWybPnn+We1aTBhaGa86AQ==
/// =fetQ
/// -----END PGP PUBLIC KEY BLOCK------"###).unwrap();
///
/// let doip_proofs: HashMap<UserID,Vec<String>> = get_keys_doip_proofs(&key).unwrap();
///
/// for (userid, proofs) in doip_proofs {
///     for proof in proofs {
///         assert_eq!(proof, "dns:openpgp.example?type=TXT");
///     }
///
///     assert_eq!(userid.name().unwrap().unwrap(), "Alice Lovelace");
///     assert_eq!(userid.email().unwrap().unwrap(), "alice@openpgp.example");
///     assert!(userid.comment().unwrap().is_none());
///     assert!(userid.uri().unwrap().is_none());
/// }
/// ```
#[allow(clippy::mutable_key_type)] // Interior Mutability of UserID in this case can be safely ignored
pub fn get_keys_doip_proofs(input_key: &Cert) -> Result<HashMap<UserID, Vec<String>>, DoipError> {
    let key_policy = &StandardPolicy::new();
    let verification_time = time::SystemTime::now();

    let valid_key = input_key
        .with_policy(key_policy, verification_time)
        .map_err(|_| DoipError::InvalidKey {
            fingerprint: input_key.fingerprint().to_hex(),
            reason: InvalidKeyReasons::KeyHasRevokedSelfSignOrNonConforming,
        })?;
    valid_key.alive().map_err(|_| DoipError::InvalidKey {
        fingerprint: input_key.fingerprint().to_hex(),
        reason: InvalidKeyReasons::KeyIsRevokedOrExpired,
    })?;

    Ok(valid_key
        .userids()
        .filter_map(|user_id| {
            let self_sig = user_id.self_signatures().next()?;

            let proofs: Vec<String> = self_sig
                .notation_data()
                .filter(|notation| {
                    notation.name().eq("proof@ariadne.id")
                        || notation.name().eq("proof@metacode.biz")
                })
                .filter_map(|notation| String::from_utf8(notation.value().to_owned()).ok())
                .collect();

            Some((user_id.userid().clone(), proofs))
        })
        .collect())
}

/// Returns Stripped Key from Armored GPG String
/// # Arguments
///
/// * `armored_key_string` - A string containg a armored PGP key
///
/// # Example
///
/// ```
/// use doip_openpgp::openpgp::read_key_from_string;
///
/// let key = read_key_from_string(r###"-----BEGIN PGP PUBLIC KEY BLOCK-----
/// Comment: Alice's OpenPGP certificate
///
/// mDMEXEcE6RYJKwYBBAHaRw8BAQdArjWwk3FAqyiFbFBKT4TzXcVBqPTB3gmzlC/U
/// b7O1u120JkFsaWNlIExvdmVsYWNlIDxhbGljZUBvcGVucGdwLmV4YW1wbGU+iJAE
/// ExYIADgCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTrhbtfozp14V6UTmPy
/// MVUMT0fjjgUCXaWfOgAKCRDyMVUMT0fjjukrAPoDnHBSogOmsHOsd9qGsiZpgRnO
/// dypvbm+QtXZqth9rvwD9HcDC0tC+PHAsO7OTh1S1TC9RiJsvawAfCPaQZoed8gK4
/// OARcRwTpEgorBgEEAZdVAQUBAQdAQv8GIa2rSTzgqbXCpDDYMiKRVitCsy203x3s
/// E9+eviIDAQgHiHgEGBYIACAWIQTrhbtfozp14V6UTmPyMVUMT0fjjgUCXEcE6QIb
/// DAAKCRDyMVUMT0fjjlnQAQDFHUs6TIcxrNTtEZFjUFm1M0PJ1Dng/cDW4xN80fsn
/// 0QEA22Kr7VkCjeAEC08VSTeV+QFsmz55/lntWkwYWhmvOgE=
/// =iIGO
/// -----END PGP PUBLIC KEY BLOCK-----"###);
/// assert_eq!(key.unwrap().fingerprint().to_hex(), "EB85BB5FA33A75E15E944E63F231550C4F47E38E");
/// ```
pub fn read_key_from_string(armored_key_string: &str) -> Result<Cert, DoipError> {
    match armored_key_string.parse::<Cert>() {
        Ok(cert) => {
            Ok(cert.strip_secret_key_material()) // We strip any secret material in case we got a private key
        }
        Err(_) => Err(DoipError::FailedToParseKey),
    }
}

/// Returns Keys found for a given `identifier` on a given `keyserver_domain` via HKP
/// # Arguments
///
/// * `identifier` - A string containing either a email address or a Key Fingerprint
/// * `keyserver_domain` - A Optional domain for the hkp lookup, defaults to keys.openpgp.org
///
/// # Example
///
/// ```
/// use doip_openpgp::openpgp::fetch_hkp;
///
/// #[tokio::main]
/// async fn main() {
///     let mut keys = fetch_hkp("EB85BB5FA33A75E15E944E63F231550C4F47E38E", None).await.unwrap();
///     assert_eq!(keys.pop().unwrap().fingerprint().to_hex(), "EB85BB5FA33A75E15E944E63F231550C4F47E38E");
/// }
/// ```
pub async fn fetch_hkp(
    identifier: &str,
    keyserver_domain: Option<&str>,
) -> Result<Vec<Cert>, DoipError> {
    let keyserver_uri: String =
        format!("hkps://{}", keyserver_domain.unwrap_or("keys.openpgp.org"));
    let keyserver = KeyServer::new(&keyserver_uri).map_err(|_| DoipError::FailedToFetchKey {
        method: "hkp".to_string(),
        location: keyserver_uri.clone(),
    })?;

    match identifier.parse() {
        Ok(KeyID::V4(data)) => {
            let keyid = KeyID::from_bytes(&data);
            match keyserver.get(keyid).await {
                Ok(sequoia_keys) => {
                    let mut certs = Vec::<Cert>::new();
                    for key in sequoia_keys {
                        match key {
                            Ok(cert) => certs.push(cert),
                            Err(_err) => {
                                return Err(DoipError::FailedToFetchKey {
                                    method: "hkp".to_string(),
                                    location: keyserver_uri.clone(),
                                });
                            }
                        }
                    }

                    Ok(certs)
                }
                Err(_) => Err(DoipError::FailedToFetchKey {
                    method: "hkp".to_string(),
                    location: keyserver_uri.clone(),
                }),
            }
        }
        Ok(_) => Err(DoipError::FailedToParseKeyIdentifier),
        Err(_) => match UserID::from_address(None, None, identifier) {
            Ok(userid) => match keyserver.search(userid).await {
                Ok(sequoia_keys) => {
                    let mut certs = Vec::<Cert>::new();
                    for key in sequoia_keys {
                        match key {
                            Ok(cert) => certs.push(cert),
                            Err(_err) => {
                                return Err(DoipError::FailedToFetchKey {
                                    method: "hkp".to_string(),
                                    location: keyserver_uri.clone(),
                                });
                            }
                        }
                    }

                    Ok(certs)
                }
                Err(_) => Err(DoipError::FailedToFetchKey {
                    method: "hkp".to_string(),
                    location: keyserver_uri.clone(),
                }),
            },
            Err(_) => Err(DoipError::FailedToParseKeyIdentifier),
        },
    }
}

/// Returns the Keys found for a given `email_address` via WKD
/// # Arguments
///
/// * `email_address` - A string containing either an email address
///
/// # Example
///
/// ```
/// use doip_openpgp::openpgp::fetch_wkd;
///
/// #[tokio::main]
/// async fn main() {
///     let mut keys = fetch_wkd("test@doip.rocks").await.unwrap();
///     assert_eq!(keys.pop().unwrap().fingerprint().to_hex(), "3637202523E7C1309AB79E99EF2DC5827B445F4B");
/// }
/// ```
pub async fn fetch_wkd(email_address: &str) -> Result<Vec<Cert>, DoipError> {
    let keys_result = sequoia_net::wkd::get(&reqwest::Client::new(), email_address).await;

    match keys_result {
        Ok(sequoia_keys) => {
            let mut certs = Vec::<Cert>::new();
            for key in sequoia_keys {
                match key {
                    Ok(cert) => certs.push(cert),
                    Err(_err) => {
                        return Err(DoipError::new_failed_to_fetch_key_wkd(email_address));
                    }
                }
            }

            Ok(certs)
        }
        Err(_) => Err(DoipError::new_failed_to_fetch_key_wkd(email_address)),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sequoia_openpgp::cert::CertBuilder;
    use tokio::test;

    static TEST_DOIP_PUBLIC_KEY_FINGERPRINT: &str = "3637202523E7C1309AB79E99EF2DC5827B445F4B";
    static TEST_DOIP_PUBLIC_KEY_EMAIL: &str = "test@doip.rocks";

    static TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_FINGERPRINT: &str =
        "EB85BB5FA33A75E15E944E63F231550C4F47E38E";
    static TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_ARMORED_STRING: &str = r###"-----BEGIN PGP PUBLIC KEY BLOCK-----
    Comment: Alice's OpenPGP certificate
    
    mDMEXEcE6RYJKwYBBAHaRw8BAQdArjWwk3FAqyiFbFBKT4TzXcVBqPTB3gmzlC/U
    b7O1u120JkFsaWNlIExvdmVsYWNlIDxhbGljZUBvcGVucGdwLmV4YW1wbGU+iJAE
    ExYIADgCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTrhbtfozp14V6UTmPy
    MVUMT0fjjgUCXaWfOgAKCRDyMVUMT0fjjukrAPoDnHBSogOmsHOsd9qGsiZpgRnO
    dypvbm+QtXZqth9rvwD9HcDC0tC+PHAsO7OTh1S1TC9RiJsvawAfCPaQZoed8gK4
    OARcRwTpEgorBgEEAZdVAQUBAQdAQv8GIa2rSTzgqbXCpDDYMiKRVitCsy203x3s
    E9+eviIDAQgHiHgEGBYIACAWIQTrhbtfozp14V6UTmPyMVUMT0fjjgUCXEcE6QIb
    DAAKCRDyMVUMT0fjjlnQAQDFHUs6TIcxrNTtEZFjUFm1M0PJ1Dng/cDW4xN80fsn
    0QEA22Kr7VkCjeAEC08VSTeV+QFsmz55/lntWkwYWhmvOgE=
    =iIGO
    -----END PGP PUBLIC KEY BLOCK-----"###;

    static TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_WITH_NOTATION_ARMORED_STRING: &str = r###"-----BEGIN PGP PUBLIC KEY BLOCK-----
    Comment: Alice's OpenPGP certificate with added notations and proofs

    mDMEXEcE6RYJKwYBBAHaRw8BAQdArjWwk3FAqyiFbFBKT4TzXcVBqPTB3gmzlC/U
    b7O1u120JkFsaWNlIExvdmVsYWNlIDxhbGljZUBvcGVucGdwLmV4YW1wbGU+iPIE
    ExYIAJoCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTrhbtfozp14V6UTmPy
    MVUMT0fjjgUCY/inQSsUgAAAAAAXAAtub3RfYV9wcm9vZkBub3RfYV9wcm9vZm5v
    dF9hX3Byb29mNRSAAAAAABAAHHByb29mQGFyaWFkbmUuaWRkbnM6b3BlbnBncC5l
    eGFtcGxlP3R5cGU9VFhUAAoJEPIxVQxPR+OOghcA/3jrFay1wXhGRcc6V6BvnpXZ
    /kNQvpjWXpWUnQ4jxpR0AP9RgvBLNETvyHb/eA4k9PS0w4VumSGPE+cCeMMVnXvw
    Arg4BFxHBOkSCisGAQQBl1UBBQEBB0BC/wYhratJPOCptcKkMNgyIpFWK0KzLbTf
    HewT356+IgMBCAeIeAQYFggAIBYhBOuFu1+jOnXhXpROY/IxVQxPR+OOBQJcRwTp
    AhsMAAoJEPIxVQxPR+OOWdABAMUdSzpMhzGs1O0RkWNQWbUzQ8nUOeD9wNbjE3zR
    +yfRAQDbYqvtWQKN4AQLTxVJN5X5AWybPnn+We1aTBhaGa86AQ==
    =fetQ
    -----END PGP PUBLIC KEY BLOCK------"###;

    #[test]
    async fn fetch_hkp_gets_a_key_from_default_with_fingerprint() {
        let mut keys: Vec<Cert> = fetch_hkp(TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_FINGERPRINT, None)
            .await
            .unwrap();

        assert_eq!(
            keys.pop().unwrap().fingerprint().to_hex(),
            TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_FINGERPRINT
        )
    }

    #[test]
    async fn fetch_hkp_fails_to_parse_identifier_and_returns_error() {
        let keys: Result<Vec<Cert>, DoipError> = fetch_hkp("NOTAKEYIDENTIFIER", None).await;

        assert!(keys.is_err());
        assert_eq!(
            keys.unwrap_err().to_string(),
            DoipError::FailedToParseKeyIdentifier.to_string()
        );
    }

    #[test]
    async fn fetch_hkp_fails_to_setup_keyserver_and_returns_error() {
        let keys: Result<Vec<Cert>, DoipError> = fetch_hkp(
            TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_FINGERPRINT,
            Some("fadsadasd@&£*!oo"),
        )
        .await;

        assert!(keys.is_err());
        assert_eq!(
            keys.unwrap_err().to_string(),
            DoipError::FailedToFetchKey {
                method: "hkp".to_string(),
                location: "hkps://fadsadasd@&£*!oo".to_string()
            }
            .to_string()
        );
    }

    #[test]
    async fn fetch_hkp_fails_to_get_key_and_returns_error() {
        let keys: Result<Vec<Cert>, DoipError> = fetch_hkp("foo@bar", None).await;

        assert!(keys.is_err());
        assert_eq!(
            keys.unwrap_err().to_string(),
            DoipError::FailedToFetchKey {
                method: "hkp".to_string(),
                location: "hkps://keys.openpgp.org".to_string()
            }
            .to_string()
        );
    }

    #[test]
    async fn fetch_hkp_gets_a_key_from_default_with_email() {
        let mut keys: Vec<Cert> = fetch_hkp(TEST_DOIP_PUBLIC_KEY_EMAIL, None).await.unwrap();

        assert_eq!(
            keys.pop().unwrap().fingerprint().to_hex(),
            TEST_DOIP_PUBLIC_KEY_FINGERPRINT
        )
    }

    #[test]
    async fn fetch_hkp_gets_a_key_from_keys_openpgp_org() {
        let keyserver_domain = "keys.openpgp.org";
        let mut keys: Vec<Cert> = fetch_hkp(
            TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_FINGERPRINT,
            Some(keyserver_domain),
        )
        .await
        .unwrap();

        assert_eq!(
            keys.pop().unwrap().fingerprint().to_hex(),
            TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_FINGERPRINT
        )
    }

    #[test]
    async fn fetch_wkd_gets_a_key() {
        let mut keys: Vec<Cert> = fetch_wkd(TEST_DOIP_PUBLIC_KEY_EMAIL).await.unwrap();

        assert_eq!(
            keys.pop().unwrap().fingerprint().to_hex(),
            TEST_DOIP_PUBLIC_KEY_FINGERPRINT
        )
    }

    #[test]
    async fn fetch_wkd_fails_to_get_a_key_and_returns_error() {
        let keys: Result<Vec<Cert>, DoipError> = fetch_wkd("foo@bar").await;

        assert!(keys.is_err());
        assert_eq!(
            keys.unwrap_err().to_string(),
            DoipError::FailedToFetchKey {
                method: "wkd".to_string(),
                location: "bar".to_string()
            }
            .to_string()
        );

        let keys_not_address: Result<Vec<Cert>, DoipError> = fetch_wkd("foo0bar").await;

        assert!(keys_not_address.is_err());
        assert_eq!(
            keys_not_address.unwrap_err().to_string(),
            DoipError::FailedToFetchKey {
                method: "wkd".to_string(),
                location: "N/A".to_string()
            }
            .to_string()
        );
    }

    #[test]
    async fn read_key_from_string_returns_a_cert() {
        let key: Cert =
            read_key_from_string(TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_ARMORED_STRING).unwrap();
        assert_eq!(
            TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_FINGERPRINT,
            key.fingerprint().to_hex()
        );
    }

    #[test]
    async fn read_key_from_string_returns_a_error_when_key_incorrect() {
        let key: Result<Cert, DoipError> = read_key_from_string("NOTAKEY");
        assert!(key.is_err());
        assert_eq!(
            key.unwrap_err().to_string(),
            DoipError::FailedToParseKey.to_string()
        );
    }

    #[test]
    async fn get_keys_doip_proofs_returns_notations() {
        let key: Cert =
            read_key_from_string(TEST_IETF_ALICE_SAMPLE_PUBLIC_KEY_WITH_NOTATION_ARMORED_STRING)
                .unwrap();

        let notation_proofs: HashMap<UserID, Vec<String>> = get_keys_doip_proofs(&key).unwrap();

        for (userid, proofs) in notation_proofs {
            for proof in proofs {
                assert_eq!(proof, "dns:openpgp.example?type=TXT");
            }

            assert_eq!(userid.name2().unwrap().unwrap(), "Alice Lovelace");
            assert_eq!(userid.email2().unwrap().unwrap(), "alice@openpgp.example");
            assert!(userid.comment2().unwrap().is_none());
            assert!(userid.uri2().unwrap().is_none());
        }
    }

    #[test]
    async fn get_keys_doip_proofs_returns_error_if_given_invalid_key() {
        let old_time = std::time::UNIX_EPOCH;
        let (cert, _) = CertBuilder::new()
            .add_userid("Alice")
            .set_creation_time(old_time)
            .set_validity_period(time::Duration::from_secs(1))
            .generate()
            .unwrap();

        assert!(get_keys_doip_proofs(&cert).is_err());
        assert_eq!(
            get_keys_doip_proofs(&cert).unwrap_err().to_string(),
            DoipError::InvalidKey {
                fingerprint: cert.fingerprint().to_hex(),
                reason: InvalidKeyReasons::KeyIsRevokedOrExpired
            }
            .to_string()
        );
    }
}
