# doip-openpgp

A Rust library to extract with decentralized online identity proofs from OpenPGP keys.

## Usage

A quick example to verify a Gitea account:

```rust
use std::collections::HashMap;
use doip_openpgp::openpgp::read_key_from_string;
use doip_openpgp::openpgp::get_keys_doip_proofs;
use sequoia_openpgp::packet::UserID;

let mut key = read_key_from_string(r###"-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: Alice's OpenPGP certificate with added notations and proofs
   
mDMEXEcE6RYJKwYBBAHaRw8BAQdArjWwk3FAqyiFbFBKT4TzXcVBqPTB3gmzlC/U
b7O1u120JkFsaWNlIExvdmVsYWNlIDxhbGljZUBvcGVucGdwLmV4YW1wbGU+iPIE
ExYIAJoCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTrhbtfozp14V6UTmPy
MVUMT0fjjgUCY/inQSsUgAAAAAAXAAtub3RfYV9wcm9vZkBub3RfYV9wcm9vZm5v
dF9hX3Byb29mNRSAAAAAABAAHHByb29mQGFyaWFkbmUuaWRkbnM6b3BlbnBncC5l
eGFtcGxlP3R5cGU9VFhUAAoJEPIxVQxPR+OOghcA/3jrFay1wXhGRcc6V6BvnpXZ
/kNQvpjWXpWUnQ4jxpR0AP9RgvBLNETvyHb/eA4k9PS0w4VumSGPE+cCeMMVnXvw
Arg4BFxHBOkSCisGAQQBl1UBBQEBB0BC/wYhratJPOCptcKkMNgyIpFWK0KzLbTf
HewT356+IgMBCAeIeAQYFggAIBYhBOuFu1+jOnXhXpROY/IxVQxPR+OOBQJcRwTp
AhsMAAoJEPIxVQxPR+OOWdABAMUdSzpMhzGs1O0RkWNQWbUzQ8nUOeD9wNbjE3zR
+yfRAQDbYqvtWQKN4AQLTxVJN5X5AWybPnn+We1aTBhaGa86AQ==
=fetQ
-----END PGP PUBLIC KEY BLOCK------"###).unwrap();

let doip_proofs: HashMap<UserID,Vec<String>> = get_keys_doip_proofs(&key).unwrap();

for (userid, proofs) in doip_proofs {
    for proof in proofs {
        assert_eq!(proof, "dns:openpgp.example?type=TXT");
    }

    assert_eq!(userid.name().unwrap().unwrap(), "Alice Lovelace");
    assert_eq!(userid.email().unwrap().unwrap(), "alice@openpgp.example");
    assert!(userid.comment().unwrap().is_none());
    assert!(userid.uri().unwrap().is_none());
}
```

## License

doip-openpgp is licensed under [Apache License, Version 2.0](https://codeberg.org/keyoxide/doip-rs/src/branch/main/LICENSE).